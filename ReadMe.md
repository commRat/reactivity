## Env
pipenv install python 3.8.3 django==2.2
pipenv shell

django-admin
django-admin startproject . # Start project in current dir

## Workspace
In VSC -> save workspace as (current folder)

## Server
VSCode terminal:
./manage.py runserver

## startapp
./manage.py startapp (name) # format
./manage.py startapp tweets # real
It creates new folder in project - which we can think of an object
We should add that to setting.py in main folder

python manage.py migrate
Aply changes & checks dbs

## URL routing & dynamic routing
in views.py (tweet folder) - set home_view func (print some HTML)

To urls.py (in main)
from tweets.views import home_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view)
]

## Imports
Upload function from another script like that:
- from folder.script import xfunction
Example: from urls.py (main) import 2 functions called home_view &
tweet_detail_view from tweets frolder):
- from tweets.views import home_view, tweet_detail_view

## Templates
- create an templates folder in same location as manage.py (BASE_DIR)
- in settings.py change template location to BASE_DIR
- make a home.html in template/pages folder 

## Bootstrap
In the templates folder we made base.html file, with basic bootstrap template
in home.html we extend that file with base.html

## _____________________
## _____MY PROJECT______

## Seting up env
django-admin startproject ReactGUI .
python manage.py migrate
npx create-react-app frontend

run:

./manage.py runserver ---DJANGO
cd frontend 

npm start ---REACT

## Folder setup
In main folder:
frontend -> react 
backend -> django

manage.py in main folder