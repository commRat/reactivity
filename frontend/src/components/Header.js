import PropTypes from 'prop-types'
import Button from './Button'


const Header = ({title, onAdd, showAdd}) => {
    return (
        <header className = 'header'>
            <h1 style={headingStyle}> { title } </h1>
            <Button color={showAdd ? 'red' : 'green'}
            text={showAdd ? 'Close' :'Open'} 
            onClick={onAdd}/>
        </header>
    )
}

// If there is no argument (props) given, that will show as default
Header.defaultProps = {
    title: 'Task Tracker'
}

// Secure that title is string
Header.propTypes = {
    title: PropTypes.string.isRequired,
}

const headingStyle = {
    color: 'red',
    backgroundColor: 'black'
}

export default Header

// serve -s build -p 8000
// save the build & run it on server (port) -p 8000