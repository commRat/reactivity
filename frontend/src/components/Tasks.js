import Task from './Task'

const Tasks = ({ tasks, onDelete, onToggle }) => {
    return (
        <>
            {tasks.map((task, index) => (
            <Task key={index} task={task} onDelete={onDelete} //key stands for unique key - handle the error
            onToggle={onToggle}/>
            ))}
        </>
    )
}

export default Tasks
