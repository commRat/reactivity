import { Link } from 'react-router-dom' 

// if we redirects, It does not reload, just switch the 
// instantly change page, because of react-router-dom


const Footer = () => {
    return (
        <footer>
            <p>Public domain &copy; 2021</p>
            <Link to='/about'>About</Link>
        </footer>
    )
}

export default Footer
